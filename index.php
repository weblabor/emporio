<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage clinic-template
 */
get_header(); ?>
<div class="container paddings">
	<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
		<h2><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>

		<div class="content">
			<p class="metadata"><?php the_author(); ?> | <?php the_date(); ?></p>
			<?php the_content(); ?>
		</div>
		<hr />
	<?php endwhile; ?>
	<div class="clear"></div>
	<div class="paginator">
		<?php $link = get_next_posts_link( '«' );
		if(strlen($link)>0) { ?>
			<div class="paginator-button"><?= $link ?></div>
		<?php } ?>
		<?php $link = get_previous_posts_link( '»' );
		if(strlen($link)>0) { ?>
			<div class="paginator-button"><?= $link; ?></div>
		<?php } ?>
	</div>
	<?php else : ?>
	<?php endif; ?>
</div>
<?php get_footer(); ?>