<?php get_header(); ?>

<div class="single-edecan">
	<div class="container">

		<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<?php $tags = get_the_tags();
	        $city = null;
	        if ($tags) {
	            foreach($tags as $tag) {
	                $city = $tag->name;
	                break;
	            }
	        } ?>
			<div class="row">
				<div class="col-sm-6 col-xs-12 description" style="background-image: url(<?php echo zels_get_option('transparent_icon_upload2') ?>)">
					<h1><?php the_title(); ?></h1>
					<div class="table">
						<span class="left">Ubicación</span>
						<span class="right"><?= $city; ?></span>
						<?php if(strlen(get_post_meta(get_the_ID(), 'wgc_weight', true))>0) { ?>
							<span class="left">Edad</span>
							<span class="right"><?= get_post_meta(get_the_ID(), 'wgc_weight', true); ?></span>
						<?php } ?>
						<span class="left">Medidas</span>
						<span class="right"><?= get_post_meta(get_the_ID(), 'wgc_measures', true); ?></span>
						<span class="left">Descripción</span>
						<span class="right"><?= get_post_meta(get_the_ID(), 'wgc_personality', true); ?></span>
					</div>		
				</div>
				<div class="col-sm-6 col-xs-12 photo">
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox">
							<?php global $post;
						    $imgs = get_children(array('post_parent' => $post->ID, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image','orderby' => 'menu_order', 'numberposts' => -1));
						    if ($imgs == true) {
						        $i = 0;
						        foreach($imgs as $id => $attachment) {
						            $i++;
						            $img = wp_get_attachment_image_src($id, 'full');
						            $img_url = $img[0]; ?>
						            <div class="item <?= $i==1 ? 'active' : '' ?>">
										<img src="<?= $img_url; ?>">
									</div>
						            <?php
						        }
						    } ?>
						</div>

						<!-- Controls -->
						<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
				</div>
			</div>
		<?php endwhile; ?>
		<?php else : ?>
		<?php endif; ?>
	</div>
</div>
	

<?php get_footer(); ?>