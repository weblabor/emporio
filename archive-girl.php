<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage clinic-template
 */
get_header(); ?>
<div class="container edecanes">
	
	<?php if ( !is_tag() ) { ?>
        <h2 style="margin-top: 60px;"><?= zels_get_option('home_title_2') ?></h2>
		<div class="cities">
			<a href="http://emporiomodels.com.mx/tag/chiapas/?post_type=girl">Chiapas</a>
			<a href="http://emporiomodels.com.mx/tag/tabasco/?post_type=girl">Tabasco</a>
			<a href="http://emporiomodels.com.mx/tag/quintana-roo/?post_type=girl">Quitana Roo</a>
		</div>
	<?php } else { ?>
		<h2 style="margin-top: 60px;"><?= zels_get_option('home_title_2') ?> » <?= single_tag_title(); ?></h2>
	<?php } ?>

	<div class="row">
		<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<?php $tags = get_the_tags();
	        $city = null;
	        if ($tags) {
	            foreach($tags as $tag) {
	                $city = $tag->name;
	                break;
	            }
	        } ?>
			<div class="col-lg-3 col-sm-4 col-xs-6">
				<div class="edecan">
					<img src="<?= get_the_post_thumbnail_url(null, 'full') ?>" />
					<div class="text">
						<h3><?php the_title() ?></h3>
						<span class="city"><?= $city ?></span>
					</div>
					<a href="<?php the_permalink() ?>"></a>
				</div>
			</div>
		<?php endwhile; ?>
		<div class="clear"></div>
		<div class="paginator">
			<?php $link = get_next_posts_link( '«' );
			if(strlen($link)>0) { ?>
				<div class="paginator-button"><?= $link ?></div>
			<?php } ?>
			<?php $link = get_previous_posts_link( '»' );
			if(strlen($link)>0) { ?>
				<div class="paginator-button"><?= $link; ?></div>
			<?php } ?>
		</div>
		<?php else : ?>
		<?php endif; ?>
	</div>
</div>
<?php get_footer(); ?>