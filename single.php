<?php get_header(); ?>

<div class="container paddings">
	<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
		<h1><?php the_title(); ?></h1>
		<div class="content">
			<p class="metadata"><?php the_author(); ?> | <?php the_date(); ?></p>
			<?php the_content(); ?>
		</div>
	<?php endwhile; ?>
	<?php else : ?>
	<?php endif; ?>
</div>

<?php get_footer(); ?>