	<footer>
	    <div class="container">
	        <div class="row">
	            <div class="column col-xs-12 col-md-3 logo">
	                <img src="<?php echo zels_get_option('logo_upload') ?>">
	            </div>
	            <div class="column col-xs-12 col-sm-8 col-md-6 line-left">
	            	<div class="text-1">
	            		<?= nl2br(zels_get_option('footer_1')) ?>	
	            	</div>
	            </div>
	            <div class="column col-xs-12 col-sm-4 col-md-3">
	            	<div class="text-2">
	            		<?= nl2br(zels_get_option('footer_2')) ?>
		                <div class="redes">
		                	<?php if(strlen(zels_get_option('social_twitter'))>0) { ?>
				                <a href="<?= zels_get_option('social_twitter') ?>" target="_blank">
				                    <i class="fa fa-twitter"></i>
				                </a>
			                <?php } ?>
			                <?php if(strlen(zels_get_option('social_facebook'))>0) { ?>
				                <a href="<?= zels_get_option('social_facebook') ?>" target="_blank">
				                    <i class="fa fa-facebook"></i>
				                </a>
			                <?php } ?>
			                <?php if(strlen(zels_get_option('social_youtube'))>0) { ?>
				                <a href="<?= zels_get_option('social_youtube') ?>" target="_blank">
				                    <i class="fa fa-youtube-play"></i>
				                </a>
			                <?php } ?>
		                </div>
	            	</div>
	            </div>
	        </div>
	    </div>
	</footer>
	<a class="whatsapp" href="https://api.whatsapp.com/send?phone=<?= zels_get_option('social_whatsapp_number'); ?>&text=<?= rawurlencode(zels_get_option('social_whatsapp_message')) ?>"></a>
	<?php wp_footer(); ?>
	<script
	  src="https://code.jquery.com/jquery-2.2.4.min.js"
	  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
	  crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>