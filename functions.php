<?php
register_nav_menus( array(
	'primary' => __( 'Primary Menu', 'clinic-template' ),
	//'secondary' => __( 'Secondary Menu', 'clinic-template' ),
) );

add_theme_support( 'post-thumbnails' );

/*------------------------------------------------------------------------------------------------------------------*/
/*	include some required file 
/*------------------------------------------------------------------------------------------------------------------*/ 

/**  framework.
--------------------------------------------------------------------------------------------------- */
require_once get_template_directory() . '/zels-framework/zels-framework.php';
?>