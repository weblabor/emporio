<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage clinic-template
 * @since clinic-template 1.0
 */

get_header(); ?>

<div class="container paddings">
	<div class="small-width-center">
		<div class="icono-space">
			<img src="<?php echo zels_get_option('transparent_icon_upload') ?>">
			<h2><?php the_title(); ?></h2>	
		</div>
	</div>
	<div class="content" role="main">
		<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<?php the_content(); ?>
				<?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'clinic-template' ) . '</span>', 'after' => '</div>' ) ); ?>
			</article><!-- #post-<?php the_ID(); ?> -->
		<?php endwhile; // end of the loop. ?>
	</div><!-- #content -->
</div>

<?php get_footer(); ?>