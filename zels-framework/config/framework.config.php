<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK SETTINGS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$settings      = array(
	'menu_title' => 'Plantilla',
	'menu_type'  => 'add_menu_page',
	'menu_slug'  => 'zels-framework',
	'ajax_save'  => false,
);

// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options        = array();

// ----------------------------------------
// a option section for options overview  -
// ----------------------------------------
$options[]      = array(
	'name'        => 'general',
	'title'       => 'General',
	'icon'        => 'fa fa-home',

	// begin: fields
	'fields'      => array(

		// begin: a field
		array(
			'id'      => 'logo_upload',
			'type'    => 'upload',
			'title'   => 'Logo',
			'default' => '',
			'help'    => 'Upload a site logo for your branding.',
		),

		array(
			'id'      => 'transparent_icon_upload',
			'type'    => 'upload',
			'title'   => 'Icono Transparente',
			'default' => ''
		),

		array(
			'id'      => 'transparent_icon_upload2',
			'type'    => 'upload',
			'title'   => 'Icono Transparente 2',
			'default' => ''
		),

		array(
			'id'          => 'home_title',
			'type'        => 'wysiwyg',
			'title'       => 'Titulo de inicio',
			'default'     => '¿Quiénes <b>somos</b>?',
		),

		array(
			'id'    => 'home_text',
			'type'  => 'textarea',
			'title' => 'Texto de inicio',
			'default'  => 'Somos una empresa con más de 14 años de experiencia en la promoción de marcas y  negocios. Tenemos como principal objetivo proporcionar a tu empresa estrategias efectivas de marketing que sumen valor y contribuyan a la construcción de tu marca, a través de impactos emocionales al consumidor.',
		),

		array(
			'id'      => 'home_image',
			'type'    => 'upload',
			'title'   => 'Imagen de inicio',
			'default' => ''
		),

		array(
			'id'          => 'column_1_home_title',
			'type'        => 'wysiwyg',
			'title'       => 'Titulo de columna 1 en inicio',
			'default'     => 'Agencia de <b>Edecanes</b>',
		),

		array(
			'id'    => 'column_1_home_text',
			'type'  => 'textarea',
			'title' => 'Texto de columna 1 en inicio',
			'default'  => 'Somos una empresa con más de 14 años de experiencia en la promoción de marcas y  negocios. Tenemos como principal objetivo proporcionar a tu empresa estrategias efectivas de marketing que sumen valor y contribuyan a la construcción de tu marca, a través de impactos emocionales al consumidor.',
		),

		array(
			'id'          => 'column_2_home_title',
			'type'        => 'wysiwyg',
			'title'       => 'Titulo de columna 2 en inicio',
			'default'     => 'Suministro de <b>Eventos</b>',
		),

		array(
			'id'    => 'column_2_home_text',
			'type'  => 'textarea',
			'title' => 'Texto de columna 2 en inicio',
			'default'  => 'Contamos con la infraestructura y los elementos que necesitas para emprender una acertada campaña publicitaria.',
		),

		array(
			'id'          => 'column_3_home_title',
			'type'        => 'wysiwyg',
			'title'       => 'Titulo de columna 1 en inicio',
			'default'     => 'Activaciones <b>BTL</b>',
		),

		array(
			'id'    => 'column_3_home_text',
			'type'  => 'textarea',
			'title' => 'Texto de columna 3 en inicio',
			'default'  => 'Activa tu negocio y dale a tus clientes una nueva experiencia de tu marca con una estrategia Below The Line.',
		),

		array(
			'id'    => 'home_title_2',
			'type'  => 'wysiwyg',
			'title' => 'Titulo para modelos en inicio',
			'default'  => 'Edecanes <b>AAA</b>',
		),

		array(
			'id'    => 'home_text_2',
			'type'  => 'wysiwyg',
			'title' => 'Texto de página de inicio hasta abajo',
			'default'  => '<b>La calidad, puntualidad y excelencia</b> en cada servicio, nos caracteriza en nuestro ramo.',
		),

		array(
			'id'      => 'favicon_upload',
			'type'    => 'upload',
			'title'   => 'Upload Favicon',
			'after'    => '<p class="cs-text-info">Upload a site favicon for your branding.</p>',
		),

	), // end field
);

$options[]      = array(
	'name'        => 'mapa',
	'title'       => 'Mapa',
	'icon'        => 'fa fa-map-marker',

	// begin: fields
	'fields'      => array(

		array(
			'id'              => 'branchs',
			'type'            => 'group',
			'title'           => 'Agregar sucursal',
			'button_title'    => 'Agregar sucursal',
			'accordion_title' => 'Agregar nueva sucursal',
			'fields'          => array(

				array(
					'id'          => 'city',
					'type'        => 'text',
					'title'       => 'Ciudad',
				),

				array(
					'id'          => 'state',
					'type'        => 'text',
					'title'       => 'Estado',
				),

				array(
					'id'          => 'phone',
					'type'        => 'text',
					'title'       => 'Teléfono',
				),

				array(
					'id'          => 'address',
					'type'        => 'text',
					'title'       => 'Dirección',
				),

				array(
					'id'          => 'location',
					'type'        => 'text',
					'title'       => 'Puntos de Ubicación',
				),

				array(
					'id'          => 'location_zoom',
					'type'        => 'text',
					'title'       => 'Zoom del mapa',
				),

			),
		),
	)
);

/**  footer.
--------------------------------------------------------------------------------------------------- */
$options[]      = array(
	'name'        => 'footer',
	'title'       => 'Footer',
	'icon'        => 'fa fa-copyright',

	// begin: fields
	'fields'      => array(

		array(
			'id'          => 'footer_1',
			'type'        => 'wysiwyg',
			'title'       => 'Titulo en footer',
			'default'     => 'EMPORIO DANIEL TOVILLA © 2016<br/>Avenida Gustavo López Solis #209 Col. Los Presidentes<br /><b>Tuxtla Gutiérrez, Chiapas</b><br />Desarrollado por <a href="http://www.weblabor.mx">Weblabor</a>',
		),

		array(
			'id'          => 'footer_2',
			'type'        => 'wysiwyg',
			'title'       => 'Titulo en footer 2',
			'default'     => 'danieltovilla@hotmail.com<br />044 961 304 2530',
		),

		array(
			'id'      => 'custom_css',
			'type'    => 'textarea',
			'title'   => 'Add custom css code here',
		),

	)
);

/**  social.
--------------------------------------------------------------------------------------------------- */
$options[]      = array(
	'name'        => 'awesocial',
	'title'       => 'Social',
	'icon'        => 'fa fa-plus',

	// begin: fields
	'fields'      => array(

		array(
			'id'          => 'social_facebook',
			'type'        => 'text',
			'title'       => 'Facebook',
			'default'     => '#',
		),
		array(
			'id'          => 'social_twitter',
			'type'        => 'text',
			'title'       => 'Twitter',
			'default'     => '#',
		),
		array(
			'id'          => 'social_youtube',
			'type'        => 'text',
			'title'       => 'Youtube',
			'default'     => '#',
		),
		array(
			'id'          => 'social_whatsapp_number',
			'type'        => 'text',
			'title'       => 'Whats App Cell Phone',
			'default'     => '#',
		),
		array(
			'id'          => 'social_whatsapp_message',
			'type'        => 'text',
			'title'       => 'Whats App Message',
			'default'     => '#',
		),
	)
);


Zels_Framework::instance( $settings, $options );
