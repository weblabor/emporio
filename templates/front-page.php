<?php
/**
 * The template for displaying home page content.
 * Template Name: Front Page
 */

get_header(); ?>

<div class="front-page">
	<?php show_sliders(); ?>
	<div class="gradient"></div>

	<div class="nosotros">
		<h2><?= zels_get_option('home_title') ?></h2>
		<div class="icono-space">
			<img src="<?php echo zels_get_option('transparent_icon_upload') ?>">
			<p><?= nl2br(zels_get_option('home_text')) ?></p>	
		</div>
	</div>

	<img class="slider" src="<?php echo zels_get_option('home_image') ?>">

	<div class="container">
		<div class="columnas">
			<div class="row">
				<div class="col-sm-4">
					<h3><?= zels_get_option('column_1_home_title') ?></h3>
					<p><?= nl2br(zels_get_option('column_1_home_text')) ?></p>
				</div>
				<div class="col-sm-4">
					<h3><?= zels_get_option('column_2_home_title') ?></h3>
					<p><?= nl2br(zels_get_option('column_2_home_text')) ?></p>
				</div>
				<div class="col-sm-4">
					<h3><?= zels_get_option('column_3_home_title') ?></h3>
					<p><?= nl2br(zels_get_option('column_3_home_text')) ?></p>
				</div>
			</div>
		</div>
		<div class="edecanes">
			<h2><?= zels_get_option('home_title_2') ?></h2>
			<div class="row">
				<?php foreach(get_girls(8) as $girl) { ?>
					<div class="col-lg-3 col-sm-4 col-xs-6">
						<div class="edecan">
							<img src="<?= $girl['url'] ?>" />
							<div class="text">
								<h3><?= $girl['name'] ?></h3>
								<span class="city"><?= $girl['city'] ?></span>
							</div>
							<a href="<?= $girl['link'] ?>"></a>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
		<div class="cita">
			<div class="icono-space text-b">
				<img src="<?php echo zels_get_option('transparent_icon_upload') ?>">
				<p><?= nl2br(zels_get_option('home_text_2')) ?></p>	
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>