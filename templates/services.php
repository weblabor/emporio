<?php
/**
 * The template for displaying home page content.
 * Template Name: Services
 */

get_header(); ?>

<div class="container services paddings">
	<div class="small-width-center">
		<div class="icono-space">
			<img src="<?php echo zels_get_option('transparent_icon_upload') ?>">
			<h2><?php the_title(); ?></h2>	
		</div>
	</div>
	<div class="row">
		<?php 
		$my_wp_query = new WP_Query();
		$all_wp_pages = $my_wp_query->query(array('post_type' => 'page', 'posts_per_page' => '-1'));
		$childrens = get_page_children(get_the_ID(), $all_wp_pages); 
		$cont = 1;
		foreach ($childrens as $children) { 
		?>
			<div class="col-lg-4 col-md-6">
				<?php $post = get_post($children->ID); ?>
				<div class="unit" style="background-image: url(<?= get_the_post_thumbnail_url(null, 'medium_large'); ?>)">
					<h3><?php echo $children->post_title; ?></h3>
					<a href="<?php echo get_permalink() ?>"></a>
				</div>
			</div>
		<?php 
			$cont++;
		} ?>
	</div>
</div>

<?php get_footer(); ?>