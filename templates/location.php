<?php
/**
 * The template for displaying home page content.
 * Template Name: Location
 */

get_header(); ?>
<script src="https://maps.google.com/maps/api/js?key=AIzaSyAsi6YqwsrGrGR4Y67qTNkBY9NdoVbB82s"></script>
<script src="https://hpneo.github.io/gmaps/gmaps.js"></script>

	<div class="container page-info">
        <div class="small-width-center">
            <div class="icono-space">
                <img src="<?php echo zels_get_option('transparent_icon_upload') ?>">
                <h2><?php the_title(); ?></h2>  
            </div>
        </div>
        <div id="map0" class="map" style="height: 400px; margin-bottom: 30px;"></div>
        <?php
            $value = zels_get_option('branchs')[3];
            $coordenadas = $value['location'];
            $coordenadas = explode(',', $coordenadas);
        ?>
        <script>
            var map = new GMaps({
                el: '#map0',
                lat: <?= $coordenadas[0] ?>,
                lng: <?= $coordenadas[1] ?>,
                zoom: <?= $value['location_zoom']; ?>
            });
            map.addMarker({
                lat: <?= $coordenadas[0] ?>,
                lng: <?= $coordenadas[1] ?>
            });
        </script>
        <div class="content">
            <?php while ( have_posts() ) : the_post(); ?>
                <?php the_content(); ?>
            <?php endwhile; // end of the loop. ?>
        </div>
        <h1>Sucursales</h1>
        <div class="row sucursales">
            <?php $cont= 0; foreach ( zels_get_option('branchs') as $value) { $cont++; ?>
                <div class="col-sm-4">
                    <div class="sucursal">
                        <h3><?= $value['city'] ?></h3>
                        <h4><?= $value['state'] ?></h4>
                        <p><img src="https://cdn.icon-icons.com/icons2/840/PNG/128/Whatsapp_icon-icons.com_66931.png" width="20" style="margin: -5px; margin-right: 5px;" /><b>Teléfono</b> <?= $value['phone'] ?></p>
                        <p><b>Dirección</b> <?= $value['address'] ?></p>
                        <div id="map<?=$cont;?>" class="map"></div>
                        <?php
                            $coordenadas = $value['location'];
                            $coordenadas = explode(',', $coordenadas);
                        ?>
                        <script>
                            var map = new GMaps({
                                el: '#map<?=$cont;?>',
                                lat: <?= $coordenadas[0] ?>,
                                lng: <?= $coordenadas[1] ?>,
                                zoom: <?= $value['location_zoom']; ?>
                            });
                            map.addMarker({
                                lat: <?= $coordenadas[0] ?>,
                                lng: <?= $coordenadas[1] ?>
                            });
                        </script>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

<?php get_footer(); ?>                